class StudentModel {
  final int id;
  final String stud_name;
  final String stud_div;
  final int stud_marks;

  StudentModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        stud_name = json['stud_name'],
        stud_div = json['stud_div'],
        stud_marks = json['stud_marks'];
}
