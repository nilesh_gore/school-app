import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:school_app/network_request.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              child: Icon(
                Icons.verified_user,
                size: 200,
                color: Colors.teal,
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                'Student App',
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                controller: nameController,
                maxLength: 100,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Username',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                maxLength: 100,
                keyboardType: TextInputType.text,
                controller: passwordController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Password'),
              ),
            ),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.blue,
                child: Text('Login'),
                onPressed: () {
                  doAuth(context, nameController.text, passwordController.text);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  doAuth(BuildContext context, String userName, String password) async {
    var data = await NetworkRequestProvider.postAuth(userName, password);
    if (data.length > 0) {
      _showToast("successful");
      setValueSP(data[0].id);
      Navigator.of(context).pushReplacementNamed('/dashboard');
    } else {
      _showToast("failed");
    }
  }

  Future<int> getValueSP() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt('isLogin') ?? 0;
  }

  setValueSP(int value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('isLogin', value);
  }

  void _showToast(String str) {
    Fluttertoast.showToast(
        msg: str,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey.shade300,
        textColor: Colors.black38,
        fontSize: 16.0);
  }
}
