import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:school_app/auth_model.dart';

class NetworkRequestProvider {
  static final baseUrl = "http://192.168.2.26:3003";

  static Future<List<AuthModel>> postAuth(
      String userName, String password) async {
    List<AuthModel> list;
    final apiResponse = await http.post(baseUrl + "/auth/",
        body: jsonEncode(<String, String>{
          "user_name": userName,
          "user_password": password
        }));
    print(apiResponse.body);
    if (apiResponse.statusCode == 200) {
      var data = json.decode(apiResponse.body);
      var rest = data as List;
      list = rest.map<AuthModel>((json) => AuthModel.fromJson(json)).toList();
    }
    return list;
  }

  static dynamic getStudents() async {
    final apiResponse = await http.get(baseUrl + "/students");
    print(apiResponse.body);
    return json.decode(apiResponse.body);
  }

  static dynamic postStudents(String name, String div, int marks) async {
    String data = "";
    final apiResponse = await http.post(baseUrl + "/addStudent/",
        body: jsonEncode(<String, String>{
          "stud_name": name,
          "stud_div": div,
          "stud_marks": marks.toString()
        }));
    print(apiResponse.body);
    if (json.decode(apiResponse.body)[0] == "Student added successfully!") {
      data = json.decode(apiResponse.body)[0];
    }
    return data;
  }
}
