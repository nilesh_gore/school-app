import 'package:flutter/material.dart';
import 'package:school_app/login_screen.dart';
import 'package:school_app/main_screen.dart';
import 'package:school_app/add_student.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Set default home.
  Widget _defaultHome = LoginPage();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  int result = prefs.getInt('isLogin') ?? 0;

  // Get result of the login function.
  if (!(result == 0)) {
    _defaultHome = MainScreen();
  }

  runApp(MaterialApp(
    title: 'School App',
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primarySwatch: Colors.deepOrange,
    ),
    home: _defaultHome,
    routes: <String, WidgetBuilder>{
      // Set routes for using the Navigator.
      '/dashboard': (BuildContext context) => MainScreen(),
      '/login': (BuildContext context) => LoginPage(),
      '/add_student': (BuildContext context) => AddStudent()
    },
  ));
}
