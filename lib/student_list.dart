import 'package:flutter/material.dart';
import 'package:school_app/network_request.dart';
import 'package:school_app/student_model.dart';

class StudentList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StudentListState();
  }
}

class _StudentListState extends State<StudentList> {
  List<StudentModel> students = List();

  @override
  void initState() {
    super.initState();
    fetchStudents();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: students == null ? 0 : students.length,
        itemBuilder: (context, index) {
          return StudentTile(students, index);
        });
  }

  fetchStudents() async {
    var root = await NetworkRequestProvider.getStudents();
    setState(() {
      List<dynamic> data = root;
      data.forEach((element) {
        students.add(StudentModel.fromJson(element));
      });
    });
  }
}

class StudentTile extends StatelessWidget {
  final List<StudentModel> students;
  final index;

  const StudentTile(this.students, this.index);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: CircleAvatar(
              radius: 30,
              child: Text(
                (index + 1).toString(),
              ),
            ),
            title: Text(
              students[index].stud_name,
              style: TextStyle(
                  fontSize: 21,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Div   : " + students[index].stud_div,
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500),
                ),
                Text(
                  "marks : " + students[index].stud_marks.toString(),
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
          ButtonTheme(
            child: ButtonBar(
              children: [
                FlatButton(onPressed: () {}, child: Text("Update")),
                FlatButton(onPressed: () {}, child: Text("Delete"))
              ],
            ),
          )
        ],
      ),
    );
  }
}
