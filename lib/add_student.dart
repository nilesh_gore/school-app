import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:school_app/network_request.dart';

class AddStudent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AddStudentState();
  }
}

class _AddStudentState extends State<AddStudent> {
  TextEditingController nameController = TextEditingController();
  TextEditingController divController = TextEditingController();
  TextEditingController marksController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Student"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              child: Icon(
                Icons.person_add_sharp,
                size: 200,
                color: Colors.teal,
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                "Add Student",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                maxLength: 100,
                keyboardType: TextInputType.name,
                controller: nameController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Name"),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                maxLength: 1,
                keyboardType: TextInputType.name,
                controller: divController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Division"),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                maxLength: 3,
                keyboardType: TextInputType.number,
                controller: marksController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Marks"),
              ),
            ),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.blue,
                child: Text("Save"),
                onPressed: () {
                  doSendData(context, nameController.text, divController.text,
                      int.parse(marksController.text));
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  doSendData(BuildContext context, String name, String div, int marks) async {
    var data = await NetworkRequestProvider.postStudents(name, div, marks);
    if (data.length > 0) {
      _showToast(data.toString());
    } else {
      _showToast("Error");
    }
  }

  void _showToast(String str) {
    Fluttertoast.showToast(
        msg: str,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey.shade300,
        textColor: Colors.black38,
        fontSize: 16.0);
  }
}
