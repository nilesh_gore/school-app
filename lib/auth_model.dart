class AuthModel {
  final int id;
  final String user_name;
  final String user_email;
  final String user_password;

  AuthModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        user_name = json['user_name'],
        user_email = json['user_email'],
        user_password = json['user_password'];
}
