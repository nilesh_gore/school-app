import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_app/student_list.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      drawer: Drawer(
        child: setDrawer(context),
      ),
      body: Center(
        child: StudentList(),
      ),
    );
  }

  Widget setDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("Iam"),
            accountEmail: Text("iam@abc.com"),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.white,
              child: Text(
                "iAM",
                style: TextStyle(fontSize: 25),
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text("Dashboard"),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.person_add_sharp),
            title: Text("Add Student"),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/add_student');
            },
          )
        ],
      ),
    );
  }
}
